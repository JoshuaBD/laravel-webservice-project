<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Examen extends Model
{
    public function users()
    {
    	return $this->belongsTo(User::class);
    }

    public function preguntes()
	{
		return $this->hasMany(Pregunte::class);
    }
}
