<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Examen;
use App\Pregunte;
use DB;

class GeneralController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        if(isset($user)) {
            if($user->rol == 1) {
                $examens = Examen::all();
            } else {
                $examens = DB::table('examens')->where('model', '=', '1')->get();
            }
            return view('welcome',compact('user','examens'));
        }

        return redirect("/login");
    }

    public function rolAuth() {
        $user = Auth::user();
        return view('create',compact('user'));
    }

    public function rank()
    {
        $examens = DB::table('examens')->where('model', '=', '0')->orderBy('id', 'desc', 'puntuacio', 'desc')->get();
        return view('rank',compact('examens'));
    }

    public function createExamen(Request $request)
    {
        $maxID = DB::table('examens')->max("id");

        $examen = new Examen();
        $examen->id = $maxID+1;

        $examen->user_id = Auth::id();
        $examen->intent = 0;
        $examen->puntuacio = 0;
        $examen->model = true;
        $examen->save();
        return redirect('/');

    }

    public function createPregunta(Request $request, Examen $examen, Pregunte $pregunta)
    {
        $pregunta = new Pregunte();
        $pregunta->examen_id = $examen->id;
        $pregunta->enunciat = $request->enunciat;
        $pregunta->resposta = $request->resposta;
        $pregunta->puntuacio = $request->puntuacio;
        $pregunta->save();

        return redirect("/examen/$examen->id");
    }

    public function editarPregunta(Request $request, Pregunte $pregunta)
    {
        $pregunta->examen_id = $pregunta->examen_id;
        $pregunta->enunciat = $request->enunciat;
        $pregunta->resposta = $request->resposta;
        $pregunta->puntuacio = $request->puntuacio;
        $pregunta->save();

        return redirect("/examen/$pregunta->examen_id");
    }

    public function initPregunta(Examen $examen)
    {
    	$user = Auth::user();
        return view('createPregunta', compact('examen', 'user'));
    }

    public function modExamen(Request $request, Examen $examen)
    {
    	if(isset($_POST['delete'])) {
    		$examen->delete();
    		return redirect('/');
    	} else {
            $user = Auth::user();
            return view('preguntes', compact('examen', 'user'));
        }
    }

    public function modPregunta(Request $request, Pregunte $pregunta)
    {
    	if(isset($_POST['delete'])) {
    		$pregunta->delete();
    		return redirect("/examen/$pregunta->examen_id");
    	} else {
            $user = Auth::user();
            return view('createPregunta', compact('pregunta', 'user'));
        }
    }

    public function doExamen(Request $Request, Examen $examen) {
        $user = Auth::user();
        $maxintent = DB::table('examens')->where([
            ['user_id', '=', $user->id],
            ['id', '=', $examen->id],
        ])->max('intent');


        if($maxintent < 3) {
            return view('doExamen', compact('examen', 'user'));
        } else {
            return redirect('/');
        }
    }

    public function finalitzar(Request $request, Examen $examen)
    {
        $user = Auth::user();
        $maxid = DB::table('examens')->where([
            ['user_id', '=', $user->id],
            ['id', '=', $examen->id],
        ])->max('intent');

        $intentEx = new Examen();
        $intentEx->id = $examen->id;
        $intentEx->user_id = $user->id;
        $intentEx->intent = $maxid+1;
        $intentEx->model = false;

        $puntuacio = 0;
        $count = 0;

    	foreach($examen->preguntes as $pregunta) {
            $string = "resposta" . $count;
            if($pregunta->resposta == $request->$string) {
                $puntuacio = $puntuacio + $pregunta->puntuacio;
            }
            $count++;
        }

        $intentEx->puntuacio = $puntuacio;
        $intentEx->save();
        return redirect("/");
    }
}
