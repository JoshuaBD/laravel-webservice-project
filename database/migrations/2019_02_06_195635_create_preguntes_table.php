<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreguntesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preguntes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('examen_id')->references('id')->on('examens');
            $table->string('enunciat');
            /* $table->string('opcions'); */
            $table->string('resposta');
            $table->integer('puntuacio');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
