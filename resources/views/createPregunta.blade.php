@extends('layouts.app')

@section('content')
<div class="container">
                @if (Auth::check())
                    @if ($user->rol == 2)
                        <div class="container">

                        @if(isset($pregunta))
                            <form method="POST" action="/editarPregunta/{{$pregunta->id}}">
                        @else
                            <form method="POST" action="/createPregunta/{{$examen->id}}">
                        @endif
                            @if(isset($pregunta))
                                <h2>Edita la pregunta</h2>
                            @else
                                <h2>Crea una pregunta</h2>
                            @endif

                            <p> Digues el enunciat del exercici: </p>
                            <textarea style='width:90%;' name='enunciat'>@if(isset($pregunta)){{$pregunta->enunciat}}@endif
                            </textarea>

                            {{--<p> Digues les opcions </p>
                            <textarea style='width:90%;' name='opcions'>@if(isset($pregunta)){{$pregunta->opcions}}@endif
                            </textarea>--}}

                            <p> Digues la resposta correcta </p>
                            <textarea style='width:90%;' name='resposta'>@if(isset($pregunta)){{$pregunta->resposta}}@endif</textarea>

                            <p> Digues quants punts val la pregunta </p>
                            <input type='number' name='puntuacio'
                            value=
                            @if(isset($pregunta))
                                {{$pregunta->puntuacio}}
                            @else
                                0
                            @endif/>
                            <hr>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">@if (isset($pregunta)) Editar Pregunta @else Afegir Pregunta @endif</button>
                            </div>
                        {{ csrf_field() }}
                        </form>


                        </div>
                    @else
                        <h3>No tens permisos per a accedir a aquest apartat...<a href="/login">Clica per tornar a Home</a></h3>
                    @endif
                @else
                    <h3>You need to log in. <a href="/login">Click here to login</a></h3>
                @endif

</div>
@endsection
