@extends('layouts.app')

@section('content')
<div class="container">
                @if (Auth::check())
                    @if ($user->rol == 1)
                        <div class="container">
                                <h2>Intent d'Examen</h2>

                                <form method="POST" action="/finalitzar/{{$examen->id}}">
                                <script> var i = 0; </script>
                                <p id="demo"></p>

                                @foreach($examen->preguntes as $pregunta)
                                    <script>
                                        document.getElementById("demo").innerHTML = document.getElementById("demo").innerHTML +
                                        "<p> {{$pregunta->enunciat}} </p>" +
                                        "<textarea style='width:90%;' name='resposta"+i+"'></textarea>" + "<hr/>";
                                        i++;
                                    </script>

                                @endforeach

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Finalitzar</button>
                                </div>
                            {{ csrf_field() }}
                            </form>
                        </div>
                    @else
                        <h3>No tens permisos per a accedir a aquest apartat...<a href="/login">Clica per tornar a Home</a></h3>
                    @endif
                @else
                    <h3>You need to log in. <a href="/login">Click here to login</a></h3>
                @endif

</div>
@endsection
