@extends('layouts.app')

@section('content')
<div class="container">
    @if (Auth::check())
        @if($user->rol == 2)
            <h2>Hola {{$user->name}}</h2>

            <a href="/initPregunta/{{$examen->id}}" class="btn btn-primary">Afegeix una Pregunta</a>

            <h3>Lista Preguntes</h3>

            <table class="table">
                <thead>
                    <tr>
                        <th>Enunciat</th>
                        <th>Resposta</th>
                        <th>Puntuació</th>
                    </tr>
                </thead>
            </thead>
            <tbody>@foreach($examen->preguntes as $pregunta)
                <tr>
                    <td>
                        {{$pregunta->enunciat}}
                    </td>
                    <td>
                        {{$pregunta->resposta}}
                    </td>
                    <td>
                        {{$pregunta->puntuacio}}
                    </td>

                    <td>

                        <form action="/pregunta/{{$pregunta->id}}">
                            <button type="submit" name="edit" class="btn btn-primary">Editar Pregunta</button>
                            <button type="submit" name="delete" formmethod="POST" class="btn btn-danger">Eliminar Pregunta</button>
                            {{ csrf_field() }}
                        </form>
                    </td>
                </tr>


            @endforeach</tbody>
            </table>
        @else
            <h3>No ets un professor... <a href="/">Clica per tornar al home</a></h3>
        @endif
    @else
        <h3>You need to log in. <a href="/login">Click here to login</a></h3>
    @endif

</div>
@endsection
