@extends('layouts.app')

@section('content')
<div class="container">
            @if (Auth::check())

                <h3>Ranking</h3>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID Examen</th>
                                <th>ID Usuario</th>
                                <th>Puntuació</th>
                            </tr>
                        </thead>
                    <tbody>
                        @foreach($examens as $examen)
                            <tr>
                                <td>
                                    {{$examen->id}}
                                </td>
                                <td>
                                    {{$examen->user_id}}
                                </td>
                                <td>
                                    {{$examen->puntuacio}}
                                </td>
                            </tr>


                    @endforeach</tbody>
                    </table>

                <hr/>

            @else
                <h3>You need to log in. <a href="/login">Click here to login</a></h3>
            @endif

</div>
@endsection
