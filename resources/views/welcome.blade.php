@extends('layouts.app')

@section('content')
<div class="container">
            @if (Auth::check())
                <h2>Hola {{$user->name}}</h2>
                @if($user->rol == 2)
                    <a href="/createExamen" class="btn btn-primary">Afegeix un Examen</a>
                @endif
                <h3>Ranking Examens</h3>
                    <a href="/rank" class="btn btn-primary">Ranking</a>

                <h3>Llista Examens</h3>

                    <table class="table">
                        <thead>
                            <tr>
                                @if($user->rol == 2)
                                    <th>ID Examen</th>
                                    <th>Model?</th>
                                    <th>Creació</th>
                                    <th>Modificació</th>
                                @else
                                    <th>ID Examen</th>
                                @endif
                            </tr>
                        </thead>
                    <tbody>
                        @foreach($examens as $examen)
                        @if($examen->model == 1)
                            <tr>
                                <td>
                                    {{$examen->id}}
                                </td>

                                @if($user->rol == 2)
                                    <td>
                                        És un examen model
                                    </td>
                                    <td>
                                        {{$examen->created_at}}
                                    </td>
                                    <td>
                                        {{$examen->updated_at}}
                                    </td>
                                    <td>
                                        <form action="/examen/{{$examen->id}}">
                                            <button type="submit" name="edit" class="btn btn-primary">Veure Examen</button>
                                            <button type="submit" name="delete" formmethod="POST" class="btn btn-danger">Eliminar</button>
                                            {{ csrf_field() }}
                                        </form>
                                    </td>
                                @else
                                    <td>
                                        <form action="/do/{{$examen->id}}">
                                            <button type="submit" name="do" class="btn btn-primary">Realitzar Intent</button>
                                            {{ csrf_field() }}
                                        </form>
                                    </td>
                                @endif
                            </tr>
                        @endif


                    @endforeach</tbody>
                    </table>

                <hr/>

                @if($user->rol == 1)
                <h3>Llista Intents</h3>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID Examen</th>
                                <th>Intent</th>
                                <th>Puntuació</th>
                                <th>Data Realitzada</th>
                            </tr>
                        </thead>
                    <tbody>
                        @foreach($examens as $examen)
                        @if($examen->user_id == $user->id)
                            <tr>
                                <td>
                                    {{$examen->id}}
                                </td>
                                <td>
                                    {{$examen->intent}}
                                </td>
                                <td>
                                    {{$examen->puntuacio}}
                                </td>
                                <td>
                                    {{$examen->created_at}}
                                </td>
                            </tr>
                        @endif


                        @endforeach
                    </tbody>
                    </table>
                @endif

            @else
                <h3>You need to log in. <a href="/login">Click here to login</a></h3>
            @endif

</div>
@endsection
