<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'GeneralController@index');
Route::get('/createExamen','GeneralController@createExamen');

Route::post('/examen/{examen}','GeneralController@modExamen');
Route::get('/examen/{examen}','GeneralController@modExamen');

Route::get('/initPregunta/{examen}','GeneralController@initPregunta');
Route::post('/createPregunta/{examen}','GeneralController@createPregunta');
Route::post('/editarPregunta/{pregunta}','GeneralController@editarPregunta');

Route::post('/pregunta/{pregunta}','GeneralController@modPregunta');
Route::get('/pregunta/{pregunta}','GeneralController@modPregunta');

Route::get('/do/{examen}','GeneralController@doExamen');
Route::post('/finalitzar/{examen}','GeneralController@finalitzar');

Route::get('/rank','GeneralController@rank');

Auth::routes();

